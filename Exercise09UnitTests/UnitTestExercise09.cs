﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Exercise09UnitTests
{
    [TestClass]
    public class UnitTestExercise09
    {
        private int[] numbers = { 5, 4, 1, 3, 5, 9, 8, 6, 7, 2, 0, 7 };

        [TestMethod]
        public void A()
        {
            var actual = (from n in numbers
                          where n % 2 != 0
                          select n).Count();
            Assert.AreEqual(7, actual);
        }

        [TestMethod]
        public void B()
        {
            var actual = (from n in numbers
                          where n % 2 == 0
                          select n)
                          .Distinct()
                          .Count();
            Assert.AreEqual(5, actual);
        }

        [TestMethod]
        public void C()
        {
            var actual = (from n in numbers
                          where n % 2 != 0
                          select n)
                         .Take(3)
                         .ToArray();
            int[] expected = { 5, 1, 3 };

            Assert.AreEqual(expected.Length, actual.Length);
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }

        [TestMethod]
        public void D()
        {
            var actual = (from n in numbers
                          where n % 2 != 0
                          select n)
                         .Skip(2)
                         .ToArray();
            int[] expected = { 3, 5, 9, 7, 7 };

            Assert.AreEqual(expected.Length, actual.Length);
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }

        [TestMethod]
        public void E()
        {
            var actual = (from n in numbers
                          where n % 2 != 0
                          group n by n
                          into g where g.Count() == 1
                          select g.First())
             
             .ToArray();
            int[] expected = { 1, 3, 9 };

            Assert.AreEqual(expected.Length, actual.Length);
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
            var a = new IGrouping<int, int>();
        }
    }
}
