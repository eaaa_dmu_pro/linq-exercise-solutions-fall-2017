﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercise08;
using System.Collections.Generic;

namespace Exercise08UnitTests
{
    [TestClass]
    public class UnitTestExercise08
    {
        [TestMethod]
        public void TestCustomersFromLondon()
        {
            var customers = Service.GetListOfCustomers();

            var expected = new List<Customer>
            {
                    new Customer { CustomerId = "CONSH", City = "London"    },
                    new Customer { CustomerId = "EASTC", City = "London"    },
                    new Customer { CustomerId = "NORTS", City = "London"    }
            };

            var actual = Program.CustomersFromLondon(customers);

            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }

        [TestMethod]
        public void TestCustomersNotFromLondon()
        {
            var customers = Service.GetListOfCustomers();

            var expected = new List<Customer>
            {
                    new Customer { CustomerId = "ALFKI", City = "Berlin"    },
                    new Customer { CustomerId = "BONAP", City = "Marseille" },
                    new Customer { CustomerId = "FRANS", City = "Torino"    },
                    new Customer { CustomerId = "LONEP", City = "Portland"  },
                    new Customer { CustomerId = "THEBI", City = "Portland"  }
            };

            var actual = Program.CustomersNotFromLondon(customers);

            Assert.AreEqual(expected.Count, actual.Count);

            for (int i = 0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i].CustomerId, actual[i]);
            }
        }

    }
}
