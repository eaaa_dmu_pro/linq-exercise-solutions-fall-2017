﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise01
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numbersLessThan50 = SelectIntegers(IntGeneratorService.GenerateListOfRandomInts(20), i => i < 50);
            PrintListToConsole("Alle tal mindre end 50: ", numbersLessThan50);
            List<int> evenNumbers = SelectIntegers(IntGeneratorService.GenerateListOfRandomInts(20), i => i % 2 == 0);
            PrintListToConsole("Alle lige tal: ", evenNumbers);
            List<int> squareNumbers = SelectIntegers(IntGeneratorService.GenerateListOfRandomInts(50), i =>
            {
                double sqrt = Math.Sqrt(i);
                return Math.Floor(sqrt) == sqrt;
            });
            PrintListToConsole("Alle kvadrat tal: ", squareNumbers);

            Console.WriteLine("------------------------------------");

            PrintListToConsole("Alle tal mindre end 50: ",
                IntGeneratorService.GenerateListOfRandomInts(20)
                .FindAll(i => i < 50));
            PrintListToConsole("Alle lige tal: ", 
                IntGeneratorService.GenerateListOfRandomInts(20)
                .FindAll(i => i % 2 == 0));
            PrintListToConsole("Alle kvadrat tal: ", 
                IntGeneratorService.GenerateListOfRandomInts(50)
                .FindAll(i =>
                {
                    double sqrt = Math.Sqrt(i);
                    return Math.Floor(sqrt) == sqrt;
                })
                );

            Console.WriteLine("Done...");
            Console.Read();
        }

        private static List<int> SelectIntegers(List<int> selectFrom, Predicate<int> selector)
        {
            List<int> selected = new List<int>();
            foreach (var number in selectFrom)
            {
                if (selector(number))
                {
                    selected.Add(number);
                }
            }
            return selected;
        }

        private static void PrintListToConsole(string msg, List<int> listToPrint)
        {
            Console.Write(msg);
            Console.Write("[ ");
            foreach (int i in listToPrint)
            {
                Console.Write(i + " ,");
            }
            Console.WriteLine(" ]");
        }
    }
}
