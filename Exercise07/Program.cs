﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise07
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = { 34, 8, 56, 31, 79, 150, 88, 7, 200, 47, 88, 20 };

            // a)
            Console.WriteLine("to-cifferet heltal i stigende orden");
            (from n in numbers
             where n > 9 && n < 100
             orderby n
             select n)
            .ToList()
            .ForEach(n => Console.Write($"{n}, "));

            // b)
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("to-cifferet heltal i faldende orden");
            numbers.Where(n => n > 9 && n < 100)
                   .OrderByDescending(n => n)
                   .Select(n => n)
                   .ToList()
                   .ForEach(n => Console.Write($"{n}, "));

            // c)
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Tocifferet heltal i stigende orden");
            Console.WriteLine(
                (from n in numbers
                 where n > 9 && n < 100
                 orderby n
                 select n.ToString())
                 .Aggregate((aggre, n) => aggre + ", " + n));

            // d)
            Console.WriteLine();
            Console.WriteLine();
            numbers.Where(n => n > 9 && n < 100)
                   .OrderByDescending(n => n)
                   .Select(n =>
                   {
                       if (n % 2 == 0)
                       {
                           return $"{n} lige";
                       }
                       else
                       {
                           return $"{n} ulige";
                       }
                   })
                   .ToList()
                   .ForEach(n => Console.Write($"{n}, "));

            // e)
            Console.WriteLine();
            Console.WriteLine();
            (from n in numbers
             where n > 9 && n < 100
             orderby n
             select new { Number = n, IsEven = n % 2 == 0 })
            .ToList()
            .ForEach(n => Console.Write($"{n}, "));

            Console.WriteLine();
            Console.WriteLine("Done");
            Console.Read();
        }
    }
}
