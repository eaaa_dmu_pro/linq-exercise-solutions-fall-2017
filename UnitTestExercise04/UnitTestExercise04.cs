﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercise04;

namespace UnitTestExercise04
{
    [TestClass]
    public class UnitTestExercise04
    {
        [TestMethod]
        public void TestSetAcceptedMethod()
        {
            PersonsClass persons = new PersonsClass();
            persons.SetAccepted(p => p.Score >= 6 && p.Age <= 40);

            Assert.IsTrue(persons.Persons[0].Accepted);
            Assert.IsTrue(persons.Persons[4].Accepted);
            Assert.IsTrue(persons.Persons[5].Accepted);

        }
    }
}
