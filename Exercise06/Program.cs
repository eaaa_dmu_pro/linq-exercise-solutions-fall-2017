﻿using Exercise02;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise06
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> persons = Service.GetListOfPersons();

            var personsOrderedByName = persons.OrderBy(p => p.Name);
            Console.WriteLine("Sorteret efter navn");
            personsOrderedByName.ToList<Person>().ForEach(p => Console.WriteLine(p));

            var personsOrderedNyNameSQLStyle = from p in persons
                                               orderby p.Name
                                               select p;
            Console.WriteLine();
            Console.WriteLine("Sorteret efter Navn");
            personsOrderedNyNameSQLStyle.ToList<Person>().ForEach(p => Console.WriteLine(p));

            // a)
            var orderedByScoreAscending = from p in persons
                                          orderby p.Score
                                          select p;

            Console.WriteLine();
            Console.WriteLine("Sorteret efter Score stigende orden");
            orderedByScoreAscending.ToList().ForEach(p => Console.WriteLine(p));

            // b)
            Console.WriteLine();
            Console.WriteLine("Sorteret efter Score faldende orden");
            persons.OrderByDescending(p => p.Score)
                   .Select(p => p)
                   .ToList()
                   .ForEach(person => Console.WriteLine(person));

            // c)
            Console.WriteLine();
            Console.WriteLine("Personer hvis navn starter med B");
            var nameStartsWithLetterB = from p in persons
                                        where p.Name.StartsWith("B")
                                        select p;

            foreach (var person in nameStartsWithLetterB)
            {
                Console.WriteLine(person);
            }

            // d)

            var peopleOlderThan39 = from p in persons
            where p.Age >= 40
            select p;

            Console.WriteLine();
            Console.WriteLine("Personer hvis alder er større eller lig 40");
            peopleOlderThan39.ToList().ForEach(p => {
                p.Accepted = true;
                Console.WriteLine(p);
            });
            // 
            Console.WriteLine("Done...");
            Console.Read();

        }
    }
}
