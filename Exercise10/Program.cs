﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise10
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] words = { "blueberry", "chimpanzee", "abacus", "banana",
                   "apple", "cheese" };

            var groupedByFirstLetter = from w in words
                                       orderby w
                                       group w by w[0];

            foreach (var letter in groupedByFirstLetter)
            {
                Console.WriteLine(letter.Key);
                foreach (var word in letter)
                {
                    Console.WriteLine($"\t{word}");
                }
            }

            Console.WriteLine("Done");
            Console.Read();
        }
    }
}
