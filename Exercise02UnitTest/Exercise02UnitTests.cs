﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercise02;
using System.Collections.Generic;
using System.Linq;

namespace Exercise02UnitTest
{
    [TestClass]
    public class Exercise02UnitTests
    {
        [TestMethod]
        public void FirstPersonWithAge44()
        {
            List<Person> persons = Service.GetListOfPersons();
            int result = persons.FindIndex(p => p.Age == 44);
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void FirstPersonWhereNameStartsWithS()
        {
            List<Person> persons = Service.GetListOfPersons();
            int result = persons.FindIndex(p => p.Name[0] == 'S');
            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void FirstPersonWhereNameContainsLetterIMoreThanOnce()
        {
            List<Person> persons = Service.GetListOfPersons();
            int result = persons.FindIndex(p => p.Name.Where(c => c == 'i').Count() > 1);
            Assert.AreEqual(4, result);
        }

        [TestMethod]
        public void FirstPersonWhereNumberOfCharsInNameMatchAge()
        {
            List<Person> persons = Service.GetListOfPersons();
            int result = persons.FindIndex(p => p.Name.Count() == p.Age);
            Assert.AreEqual(5, result);
        }
    }
}
