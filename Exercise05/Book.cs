﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise05
{
    public class Book
    {
        public string Author { get; set; }
        public string Name { get; set; }
        public DateTime Published { get; set; }

        public override bool Equals(object obj)
        {
            Book cmpBook = obj as Book;
            if (cmpBook == null)
            {
                return false;
            }

            return Equals(cmpBook);
        }

        public bool Equals(Book book)
        {
            return Author == book.Author && Name == book.Name && Published.Equals(book.Published);
        }
    }
}
