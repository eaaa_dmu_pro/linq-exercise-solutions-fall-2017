﻿using System;
using System.Collections.Generic;

namespace Exercise05
{
    public static class Service
    {
        public static List<Book> GetListOfBooks()
        {
            return new List<Book>
            {
              new Book { Author="McConnell",Name="Code Complete",
                         Published=new DateTime(1993,05,14) },
              new Book { Author="Sussman",Name="SICP (2nd)",
                         Published=new DateTime(1996,06,01) },
              new Book { Author="Hunt",Name="Pragmatic Programmer",
                         Published=new DateTime(1999,10,30) } };
        }
    }
}
