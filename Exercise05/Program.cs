﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise05
{
    public static class Program
    {
        static void Main(string[] args)
        {
            List<Book> books = Service.GetListOfBooks();
            List<Book> booksPublishedAfter1995 = PublishedAfter1995(books);
            List<Book> booksPublishedBetween1991And1997 = PublishedBetween1991And1997(books);
        }

        public static List<Book> PublishedBetween1991And1997(List<Book> books)
        {
            return books.FindAll(b => b.Published.Year >= 1991 && b.Published.Year < 1997);
        }

        public static List<Book> PublishedAfter1995(List<Book> books)
        {
            return books.FindAll(b => b.Published.Year > 1995);
        }
    }
}
