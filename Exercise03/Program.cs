﻿using Exercise02;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise03
{
    class Program
    {
        static void Main(string[] args)
        {
            //part a
            List<Person> persons = Service.GetListOfPersons();

            persons.ForEach(p => Console.WriteLine(p.Name));

            //part b
            List<string> words = new List<string> { "The", "answer", "my", "friend", "is", "blowin'", "in", "the", "wind" };
            string line = words.Aggregate((sentence, next) => $"{sentence} {next}");
            Console.WriteLine(line);
            Console.WriteLine("Done...");
            Console.Read();
        }

        private static void PrintName(Person person)
        {
            Console.WriteLine(person.Name);
        }
    }
}
