﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise02
{
    public static class Service
    {
        public static List<Person> GetListOfPersons()
        {
            return new List<Person>
            {
                new Person { Name = "Bent", Age = 25, Score = 9 },
                new Person { Name = "Susan", Age = 34, Score = 3 },
                new Person { Name = "Mikael", Age = 60, Score = 8 },
                new Person { Name = "Klaus", Age = 44, Score = 7 },
                new Person { Name = "Birgitte", Age = 17, Score = 10 },
                new Person { Name = "Liselotte", Age = 9, Score = 9 }
            };
        }
    }
}
