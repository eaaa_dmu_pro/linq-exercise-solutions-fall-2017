﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise02
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public int Score { get; set; }
        public bool Accepted { get; set; }

        public override string ToString()
        {
            return $"Name: {Name}, Age: {Age}, Score: {Score}, Accepted: {(Accepted?"Yes":"No")}";
        }
    }
}
