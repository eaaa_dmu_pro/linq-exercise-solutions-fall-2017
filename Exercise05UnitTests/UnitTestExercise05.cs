﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Exercise05;
using System.Collections.Generic;

namespace Exercise06UnitTests
{
    [TestClass]
    public class UnitTestExercise05
    {
        [TestMethod]
        public void TestPublishedAfter1995()
        {
            var books = Service.GetListOfBooks();
            List<Book> expected = new List<Book>
            {
                new Book { Author="Sussman",Name="SICP (2nd)",
                         Published=new DateTime(1996,06,01) },
                new Book() { Author="Hunt",Name="Pragmatic Programmer",
                         Published=new DateTime(1999,10,30) }
            };

            List<Book> actual = Program.PublishedAfter1995(books);

            Assert.AreEqual(expected.Count, actual.Count);
            for (int i = 0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }

        [TestMethod]
        public void TestPublishedBetween1991And1997()
        {
            var books = Service.GetListOfBooks();
            List<Book> expected = new List<Book>
            {
              new Book { Author="McConnell",Name="Code Complete",
                         Published=new DateTime(1993,05,14) },
              new Book { Author="Sussman",Name="SICP (2nd)",
                         Published=new DateTime(1996,06,01) },
            };

            List<Book> actual = Program.PublishedBetween1991And1997(books);
            Assert.AreEqual(expected.Count, actual.Count);
            for (int i = 0; i < expected.Count; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }
        }
    }
}

