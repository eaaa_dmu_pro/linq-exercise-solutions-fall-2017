﻿using Exercise02;
using System;
using System.Collections.Generic;

namespace Exercise04
{
    public class PersonsClass
    {
        public List<Person> Persons { get; set; }

        public PersonsClass()
        {
            Persons = Service.GetListOfPersons();
        }

        public void SetAccepted(Predicate<Person> predicate)
        {
            Persons.ForEach(p =>
            {
                if (predicate(p))
                {
                    p.Accepted = true;
                }
            });
        }

    }
}