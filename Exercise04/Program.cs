﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise04
{
    public class Program
    {
        static void Main(string[] args)
        {
            PersonsClass persons = new PersonsClass();

            persons.Persons.ForEach(person => Console.WriteLine(person));
            Console.WriteLine();

            persons.SetAccepted(p => p.Score >= 6 && p.Age <= 40);

            persons.Persons.ForEach(person => Console.WriteLine(person));

            Console.WriteLine("Done...");
            Console.Read();

        }
    }
}
