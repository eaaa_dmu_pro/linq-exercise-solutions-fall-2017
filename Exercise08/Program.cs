﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise08
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<Customer> customers = Service.GetListOfCustomers();

            var fromLondon = CustomersFromLondon(customers);

            var notFromLondon = CustomersNotFromLondon(customers);


        }

        public static List<Customer> CustomersFromLondon(List<Customer> customers)
        {
            return (from c in customers
                    where c.City == "London"
                    select c).ToList();
        }

        public static List<string> CustomersNotFromLondon(List<Customer> customers)
        {
            return customers.Where(c => c.City != "London")
                            .Select(c => c.CustomerId)
                            .ToList();
        }
    }
}
