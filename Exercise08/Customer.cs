﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise08
{
    public class Customer
    {
        public string CustomerId { get; set; }
        public string City { get; set; }

        public override string ToString()
        {
            return CustomerId + "\t" + City;
        }

        public override bool Equals(object obj)
        {
            Customer cust = obj as Customer;
            if (cust == null)
            {
                return false;
            }
            return Equals(cust);
        }

        public bool Equals(Customer customer)
        {
            return CustomerId == customer.CustomerId;
        }
    }
}
