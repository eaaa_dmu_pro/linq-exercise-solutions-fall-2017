﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise08
{
    public static class Service
    {
        public static List<Customer> GetListOfCustomers()
        {
            return new List<Customer>
                {
                    new Customer { CustomerId = "ALFKI", City = "Berlin"    },
                    new Customer { CustomerId = "BONAP", City = "Marseille" },
                    new Customer { CustomerId = "CONSH", City = "London"    },
                    new Customer { CustomerId = "EASTC", City = "London"    },
                    new Customer { CustomerId = "FRANS", City = "Torino"    },
                    new Customer { CustomerId = "LONEP", City = "Portland"  },
                    new Customer { CustomerId = "NORTS", City = "London"    },
                    new Customer { CustomerId = "THEBI", City = "Portland"  }
                };

        }

    }
}
